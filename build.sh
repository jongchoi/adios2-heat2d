#!/bin/bash
pushd simulation
make adios2
popd

pushd analysis
make adios2_file
make adios2_stream
popd
