#!/bin/bash

#METHOD=BPFile
#METHOD=SST
#METHOD=InSituMPI
METHOD=$1
[ -z $METHOD ] && METHOD=BPFile

cat <<EOF > adios2.xml
<?xml version="1.0"?>
<adios-config>
    <io name="SimulationOutput">
        <engine type="$METHOD">
        </engine>
    </io>
    <io name="AnalysisOutput">
        <engine type="BPFile">
        </engine>
    </io>
    <io name="VizInput">
        <engine type="bpfile">
        </engine>
    </io>
</adios-config>

EOF

rm -rf *.bp*
rm -f fort.*
find ./ -name *.sst | xargs rm -f


#DDT_OPT="/opt/X11/bin/xterm -e lldb -s lldb.cmd"
DDT_OPT=""

#MPICMD="mpirun --tag-output"
MPICMD="mpirun"

if [ 1 -eq 1 ]; then
$MPICMD \
    -n 4 bash -c "$DDT_OPT simulation/heatSimulation_adios2 heat 2 2 600 300 10 600 | sed 's/^/[HEAT:'\$OMPI_COMM_WORLD_RANK'] /'" :\
    -n 4 bash -c "$DDT_OPT analysis/heatAnalysis_adios2_stream heat.bp | sed 's/^/[ANAL:'\$OMPI_COMM_WORLD_RANK'] /'" |\
    tee run.log
fi

if [ 0 -eq 1 ]; then
$MPICMD \
    -n 4 bash -c "$DDT_OPT simulation/heatSimulation_adios2 heat 2 2 300 300 10 600 | sed 's/^/[HEAT:'\$OMPI_COMM_WORLD_RANK'] /'" :\
    -n 4 bash -c "$DDT_OPT adios2_reorganize heat.bp staged.bp $METHOD \"\" BPFile \"\" 4 | sed 's/^/[ANAL:'\$OMPI_COMM_WORLD_RANK'] /'" |\
    tee run.log
fi
